@extends('../layout')
@section('title')
Manage Website| BackLink Sync
@stop
@section('extra_links')
<link rel="stylesheet" type="text/css" href="../../css/metro-bootstrap.css">
<link rel="stylesheet" type="text/css" href="../../css/iconFont.min.css">
<link rel="stylesheet" type="text/css" href="../../css/metro-bootstrap-responsive.css">
 {{ HTML::style('css/home.css')}}
@stop
@section('extra_scripts')
 <script type="text/javascript" src="../../js/jquery.widget.min.js"></script> 
<script type="text/javascript" src="../../js/metro.min.js"></script>
 @stop
@section('body')

<div id="details" style="width:900px;">
      <nav class="navigation-bar">
    <nav class="navigation-bar-content">
        <div class="element">
            <a class="dropdown-toggle" href="#">METRO UI CSS</a>
            <ul class="dropdown-menu" data-role="dropdown">
                <li><a href="#">Main</a></li>
                <li><a href="#">File Open</a></li>
                <li class="divider"></li>
                <li><a href="#">Print...</a></li>
                <li class="divider"></li>
                <li><a href="#">Exit</a></li>
            </ul>
        </div>
 
        <span class="element-divider"></span>
        <a class="element brand" href="#"><span class="icon-spin"></span></a>
        <a class="element brand" href="#"><span class="icon-printer"></span></a>
        <span class="element-divider"></span>
 
        <div class="element input-element">
            <form>
                <div class="input-control text">
                    <input type="text" placeholder="Search...">
                    <button class="btn-search"></button>
                </div>
            </form>
        </div>
 
        <div class="element place-right">
            <a class="dropdown-toggle" href="#">
                <span class="icon-cog"></span>
            </a>
            <ul class="dropdown-menu place-right" data-role="dropdown">
                <li><a href="#">Products</a></li>
                <li><a href="#">Download</a></li>
                <li><a href="#">Support</a></li>
                <li><a href="#">Buy Now</a></li>
            </ul>
        </div>
        <span class="element-divider place-right"></span>
        <a class="element place-right" href="#"><span class="icon-locked-2"></span></a>
        <span class="element-divider place-right"></span>
        <button class="element image-button image-left place-right">
             {{ Auth::user()->full_name }}
            <!--<img src="images/211858_100001930891748_287895609_q.jpg"/>-->
        </button>
    </nav>
</nav>
  
       </div>
       @stop