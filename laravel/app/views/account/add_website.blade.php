@extends('../layout')
@section('title')
My Account| BackLink Sync
@stop
@section('extra_links')
{{ HTML::style('css/metro-bootstrap.css')}}
{{ HTML::style('css/iconFont.min.css')}}
{{ HTML::style('css/metro-bootstrap-responsive.css')}}
 {{ HTML::style('css/home.css')}}
@stop
@section('extra_scripts')
 <script type="text/javascript" src="../js/jquery.widget.min.js"></script> 
<script type="text/javascript" src="../js/metro.min.js"></script>
 @stop
@section('body')

<div id="details" style="width:900px;">
  <h3><a href="/"><i class="icon-arrow-left-3 fg-darker smaller"></i></a>Add My Website</h3>
<p>
{{Form::open(array('route' => 'account.insert_website'))}}
        {{ Form::label("url", "Url") }}
        {{ Form::text("url") }}<br/>
        {{ Form::label("script_url", "Url of Script") }}
        {{ Form::text("script_url") }}<br/>
        {{ Form::label("title", "Website title") }}
        {{ Form::text("title") }}<br/>
        {{ Form::label("description", "Website Description") }}
        {{ Form::text("description") }}<br/>
        {{ Form::label("industry", "Website Industry") }}
        {{ Form::password("industry") }}<br/>
        {{ Form::submit("Add") }}
        {{ Form::close() }} 
</div>
       @stop