@extends('../layout')
@section('title')
My Account| BackLink Sync
@stop
@section('extra_links')
<link rel="stylesheet" type="text/css" href="../css/metro-bootstrap.css">
<link rel="stylesheet" type="text/css" href="../css/iconFont.min.css">
<link rel="stylesheet" type="text/css" href="../css/metro-bootstrap-responsive.css">
 {{ HTML::style('css/home.css')}}
@stop
@section('extra_scripts')
 <script type="text/javascript" src="../js/jquery.widget.min.js"></script> 
<script type="text/javascript" src="../js/metro.min.js"></script>
 @stop
@section('body')

<div id="details" style="width:900px;">
      <nav class="navigation-bar dark">
    <nav class="navigation-bar-content">
        <div class="element">
            <a class="dropdown-toggle" href="#">BACKLINK SYNC</a>
            <ul class="dropdown-menu" data-role="dropdown">
                <li><a href="#">Main</a></li>
                <li><a href="#">File Open</a></li>
                <li class="divider"></li>
                <li><a href="#">Print...</a></li>
                <li class="divider"></li>
                <li><a href="#">Exit</a></li>
            </ul>
        </div>
 
        <span class="element-divider"></span>
        <a class="element brand" title="My requests" href="requests"><span class="icon-spin"></span></a>
        <a class="element brand" title="My Inbox" href="inbox"><span class="icon-mail"></span></a>
        <span class="element-divider"></span>
 
        <div class="element input-element">
            <form method="post" action="../search/index">
                <div class="input-control text">
                    <input type="text" placeholder="Search for links...">
                    <button class="btn-search"></button>
                </div>
            </form>
        </div>
 
        <div class="element place-right">
            <a class="dropdown-toggle" href="#">
                <span class="icon-cog"></span>
            </a>
            <ul class="dropdown-menu place-right" data-role="dropdown">
                <li><a href="#">Products</a></li>
                <li><a href="#">Download</a></li>
                <li><a href="#">Support</a></li>
                <li><a href="#">Buy Now</a></li>
            </ul>
        </div>
        <span class="element-divider place-right"></span>
        <a class="element place-right" href="#"><span class="icon-locked-2"></span></a>
        <span class="element-divider place-right"></span>
        <button class="element image-button image-left place-right">
             {{ Auth::user()->full_name }}
            <!--<img src="images/211858_100001930891748_287895609_q.jpg"/>-->
        </button>
    </nav>
</nav>
  <br/>
  @if (Auth::check())
  <h2>My Account</h2>
  <div class="tile double bg-amber">
          <ul class="fg-white" style="list-style-type:none; padding:0px;">
          <li class="list-group-item fg-white">
         
         {{"You are logged in"}}
         {{ Auth::user()->full_name }} <br/><br/>
            <button>{{ link_to_route('user.logout', 'Log Out') }}</button><br/>
        
            </ul>
         </div> 
    @endif
  <div class="tile live double bg-cobalt"  data-role="live-tile" data-effect="slideUpDown">
    <div class="tile-content">
      <p class="fg-white">Link Requests</p>
    </div>
     <div class="tile-content">
       <p class="fg-white">You have 3 new Requests</p>
    </div>
  </div>
  <div class="tile live double bg-cyan" data-role="live-tile" data-effect="slideUpDown">
   <div class="tile-content">
     <p class="fg-white">Inboxes(4)</p>
    </div>
     <div class="tile-content">
       <h4 class="fg-white">Hello John<h4>
       <p class="fg-white"> I would love to..</p>
    </div>
  </div>    
       <div style="width:100%; float:left;">
       <h2>Get New Links</h2>
        <div class="tile quadro bg-green">
          <p class="fg-white">Search for new links</p>
          <br/>
          <form>
                <div class="input-control text">
                    <input type="text" placeholder="Search...">
                    <button class="btn-search"></button>
                </div>
            </form>
       </div>
       <div class="tile double bg-orange">
         <p class="fg-white">...or browse by industry, country or keywords.</p>
       </div>
       </div>
       <div style="width:100%; float:left;">
        <h2>Manage Websites</h2>
         <div class="tile small bg-red">
         <form action="">
           <p class="fg-white">{{ HTML::linkRoute('account.add_website', 'Add My Website') }}</p>
         </form>
         </div>
         <div class="tile small bg-blue">
         <form action="">
           <button class="button small">Download Backlink Sync Client</button>
         </form>
          
         </div>
         
          @if (Auth::check())
           
           @endif
         
       </div>
       </div>
       </div>
       @stop