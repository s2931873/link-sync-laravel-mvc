@extends('../layout')
@section('title')
Create Account| BackLink Sync
@stop
@section('body')
<div id="details">
    <nav>
  <ul>
    <li>What we are about...</li>
    <li>Send us a message...</li>
  </ul>
  </nav>
<h2>Create Login</h2>

{{Form::open(array('route' => 'user.store'))}}
{{ Form::label("username", "Username") }}<br/>
  {{ Form::text("username") }}<br/>
{{ Form::label("password", "Password") }}<br/>
  {{ Form::password("password") }}<br/>
{{ Form::label("email", "Email") }}<br/>
  {{ Form::text("email") }}<br/>
  {{ Form::label("full_name", "Full name (eg. John Smith)") }}<br/>
  {{ Form::text("full_name") }}<br/>
  
  {{ Form::label('company', 'company:') }}  <br/>
  {{ Form::text("company") }}<br/>
  {{ Form::submit("Create") }}<br/>
  {{ Form::close() }}

@if($errors->has())
   @foreach ($errors->all() as $error)
      <div>{{ $error }}</div>
  @endforeach
@endif
@stop