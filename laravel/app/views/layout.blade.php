<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
  <link rel="icon" type="image/icon" href="/favicon.ico">
     {{ HTML::style('css/home.css')}}
@yield('extra_links')
    {{ HTML::script('http://code.jquery.com/jquery-latest.min.js')}}
    {{ HTML::script('js/home.js')}}
@yield('extra_scripts')
</head>
<body class="metro">
  <div id="top-bar">
<h1>BackLink Sync</h1>
</div>
<div id="content">
<div id="center">
<div id="center-image">
{{ HTML::image($background)}}
@yield('body')

</div>
    <footer>
<br/>
<p style="color:#fff;">Patent Pending</p>
<p style="color:#fff;">&#169; BackLink Sync | 2012 - 2014</p>
</footer>
</div>

</div>
</body>
</html>