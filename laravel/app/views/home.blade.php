@extends('layout')
@section('title')
Login or Sign Up
@stop
@section('extra_links')
<link rel="stylesheet" type="text/css" href="css/home.css">
{{ HTML::style('css/bootstrap.min.css')}}
{{ HTML::style('css/bootstrap-theme.min.css')}}
@stop
@section('extra_scripts')
 <script type="text/javascript" src="js/home.js"></script>
{{ HTML::script('js/bootstrap.min.js')}}
 @stop
@section('body')

<div id="details">
    <nav>
  <ul>
    <li>What we are about...</li>
    <li>Send us a message...</li>
  </ul>
  </nav>
  
<h3>Create Account</h3>
<p>
{{ link_to_route('user.create', 'Create Account') }}
<h3>Log in</h3>
<p>
{{Form::open(array('route' => 'user.login'))}}
        {{ Form::label("username", "Username") }}
        {{ Form::text("username") }}<br/>
        {{ Form::label("password", "Password") }}
        {{ Form::password("password") }}<br/>
        {{ Form::submit("Login") }}
        {{ Form::close() }}
  @foreach ($errors as $e)
  <p>{{$e}}</p>
  @endforeach
<h3>Change password</h3>
<p>
<form action="includes/user-man.php" method="POST">
	<input type="hidden" name="op" value="change">
	Username:<br>
	<input type="text" name="user" size="40"><br>
	Current password:<br>
	<input type="password" name="pass" size="40"><br>
	New password:<br>
	<input type="password" name="newpass" size="40"><br>
	<input type="submit" value="Change password">
</form>
  <h3>Forgot password</h3>
<p>
<form action="includes/user-man.php" method="POST">
	<input type="hidden" name="op" value="change">
	Email:<br>
	<input type="text" name="user" size="40"><br>
	<input type="submit" value="Change password">
</form>
</div>
 @stop