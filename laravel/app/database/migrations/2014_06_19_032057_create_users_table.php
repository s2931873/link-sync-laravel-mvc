<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
public function up()
	{
		Schema::create('users',
    function($table) {
     $table->increments('id');
     $table->string('username')->unique();
     $table->string('password')->index();
     $table->string('email')->unique();
     $table->string('full_name');
     $table->string('company');
     $table->datetime('date_signed_up');
     $table->string('remember_token');
     $table->timestamps();
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
