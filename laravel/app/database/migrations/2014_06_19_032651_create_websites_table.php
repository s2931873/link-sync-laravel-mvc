<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebsitesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('websites',
    function($table) {
     $table->increments('id');
     $table->integer('user_id');
     $table->string('url')->unique();
     $table->string('title');
     $table->text('desc');
     $table->string('country');
     $table->string('industry');
     $table->integer('directory'); 
     $table->timestamps();
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
