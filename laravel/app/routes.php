<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('home');
});

Route::get('user/create', array('as' => 'user.create', 'uses' =>       'UserController@create'));
    Route::post('user/store', array('as' => 'user.store', 'uses' =>       'UserController@store'));
 Route::post('user/login', array('as' => 'user.login', 'uses' =>       'UserController@login'));
Route::get('user/logout', array('as' => 'user.logout', 'uses' =>       'UserController@logout'));
Route::get('account/index', array('as' => 'account.index', 'uses' =>       'AccountController@index'));
Route::get('account/add_website', array('as' => 'account.add_website', 'uses' =>       'AccountController@add_website'));
Route::post('account/insert_website', array('as' => 'account.insert_website', 'uses' =>       'AccountController@insert_website'));
Route::get('account/manage_website/{id}', array('as' => 'account.manage_website', 'uses' =>       'AccountController@manage_website'));
Route::get('account/requests', array('as' => 'account.requests', 'uses' =>       'AccountController@requests'));
Route::get('account/inbox', array('as' => 'account.inbox', 'uses' =>       'AccountController@inbox'));
Route::get('search/index/{query}', array('as' => 'search.index', 'uses' =>       'SearchController@index'));
Route::get('directory/index', array('as' => 'directory.index', 'uses' =>       'DirectoryController@index'));