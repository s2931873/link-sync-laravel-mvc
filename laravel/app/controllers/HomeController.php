<?php

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function showWelcome()
	{
  if(!Auth::check()){
    return View::make('home');
  }else{
      //if user already logged in send them to dashboard
       $websites = Account::where('user_id', Auth::user()->id)->get();
       return View::make('account.index', compact('websites'));
    }
	}
}
