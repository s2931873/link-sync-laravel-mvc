<?php

class UserController extends \BaseController {

  //seeker create form
  public function create()
	{
  return View::make('user.create');
	}

 

	/**
	 * Store a newly created user/employer in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
     $input = Input::all();
 
       $password = $input['password'];
       $encrypted = Hash::make($password);
       //save a regular user
       $user = new User;
       $user->username = $input['username'];
       $user->password = $encrypted;
       $user->email = $input['email'];
       $user->company = $input['company'];   
      // $user->image = $input['image'];
       $user->full_name = $input['full_name'];   
       $user->date_signed_up= new DateTime('today');
       $user->remember_token = "default";
        $user->save();
   //if user is employer create foreign key
 
   
   //auto log them in
    $credentials = array(
  'username' => Input::get('username'),
  'password' => Input::get('password')
  );
   Auth::attempt($credentials);
   //redirect to home page with jobs 
   return Redirect::route('account.index');
  }
    //validate job seeker
    
  
  /**
	 * Handle - User logs in
	 *
	 * @return Response
	 */
  public function login(){
   //dont display home page if user already logged in
    
    $credentials = array(
  'username' => Input::get('username'),
  'password' => Input::get('password')
  );
   if (Auth::attempt($credentials)) {
   //successful login
      $websites = Account::where('user_id', Auth::user()->id)->get();
     return View::make('account.index', compact('websites'));
    
    }else{
     //password was wrong
     return Redirect::to(URL::previous())->withErrors('Sorry, The username or password was     incorrect. Please try again, or reset your password.');
   }
  }
  
  /**
	 * Handle - User logs out.
	 *
	 * @return Response
	 */
  public function logout(){
      Auth::logout();
      return Redirect::to(URL::previous())->WithErrors("You have logged out");  
    }
  
}