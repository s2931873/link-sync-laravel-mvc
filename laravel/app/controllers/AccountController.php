<?php

class AccountController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
      if (Auth::check()){
         $websites = Account::where('user_id', Auth::user()->id)->get();
		return View::make('account.index', compact('websites'));
	}else{
        $websites = Account::where('user_id', Auth::user()->id)->getAll();
    		return View::make('home');
  }
}
  
  	public function add_website()
	{
      if (Auth::check()){
		return View::make('account.add_website');
	}else{
    		return View::make('home');
  }
}
  public function insert_website()
	{
      if (Auth::check()){
		  //add the new website
       $input = Input::all();
       //save the website
       $account = new Account;
       $account->user_id = Auth::user()->id; 
       $account->url = $input['url'];
       $account->title = $input['title'];
       $account->desc = $input['description'];
       $account->country = 'AUS';
       $account->industry = $input['industry'];   
       $account->directory= 1;
       $account->save();
       return View::make('account.index')->with('message','Success');
        
	}else{
    		return View::make('home');
  }
}
  public function manage_website($id)
	{
      if (Auth::check()){
         return View::make('account.manage_website');
        }else{
    		return View::make('home');
  }
  }
  public function requests(){
    	return View::make('account.requests');
    
  }
  public function inbox(){
    return View::make('account.inbox');
  }
}
